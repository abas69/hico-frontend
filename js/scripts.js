var body = document.querySelector('body');
var overlay = document.querySelector('.overlay');


if (document.querySelector('.btn-mobile-menu') != null) {
    var btnMobileMenu = document.querySelector('.btn-mobile-menu');
    var sidebarMenu = document.getElementById('sidebar-menu');
    var menuOverlay = document.querySelector('.overlay-back');
    var btnCloseSidebar = document.querySelector('.btn-close-menu');
    btnMobileMenu.onclick = () => {
        menuOverlay.classList.add('active');
        sidebarMenu.classList.add('active');
    }
    menuOverlay.onclick = () => {
        menuOverlay.classList.remove('active');
        sidebarMenu.classList.remove('active');
    }
    btnCloseSidebar.onclick = () => {
        menuOverlay.classList.remove('active');
        sidebarMenu.classList.remove('active');
    }
}


if (document.getElementById('navbarDash') != null) {
    var navbarDash = document.getElementById('navbarDash');
    var sidebarDash = document.getElementById('sidebarDash');
    var contentDash = document.getElementById('contentDash');
    var sidebarCollapse = document.getElementById('sidebarCollapse');
    var sidebarOverlay = document.querySelector('.sidebar-overlay');
    var btnCloseSidebar = document.querySelector('.btn-close-sidebar');

    sidebarCollapse.onclick = () => {
        navbarDash.classList.toggle('active')
        sidebarDash.classList.toggle('active')
        contentDash.classList.toggle('active')
        sidebarCollapse.classList.toggle('active');
        sidebarOverlay.style.display = "block"
    }
    sidebarOverlay.onclick = () => {
        sidebarOverlay.style.display = "none"
        navbarDash.classList.remove('active')
        sidebarDash.classList.remove('active')
        contentDash.classList.remove('active')
    }
    btnCloseSidebar.onclick = () => {
        sidebarOverlay.style.display = "none"
        navbarDash.classList.remove('active')
        sidebarDash.classList.remove('active')
        contentDash.classList.remove('active')
    }
}


if (document.getElementById('select-login-users') != null) {
    document.getElementById('select-login-users').addEventListener('change', function () {
        var showCodeMeli = this.value == 0 ? 'block' : 'none';
        document.getElementById('code-meli').style.display = showCodeMeli;
    })
}


// Disabled Modal Haj Sakhteman colse
// $('#hajm-sakhteman').modal({ backdrop: 'static', keyboard: false })  